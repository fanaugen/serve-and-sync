/*
 * Serve static files, and inject changes (Live Reload) via the browser-sync plugin
 * Usage: node server.js &
 * Requirements: the required node modules
 * TODO make box-independent
 */

var connect     = require('connect');
var serveStatic = require('serve-static');
var browserSync = require('browser-sync');
var middleware  = require('connect-browser-sync');

var app = connect();

// init browser sync, inject the scripts via the connect middleware
var bs_files = [
  "**/*.html",
  "**/*.css"
],
bs_options = {
  host: "codebar-box-107638.euw1.nitrousbox.com",
  ports: {
    min: "3003"
  }
};
var bs = browserSync.init(bs_files, bs_options);
app.use(middleware(bs));

// serve static files
app.use(serveStatic(__dirname));

app.listen(8080);
