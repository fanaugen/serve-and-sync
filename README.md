# serve static files with browser-sync

## method 1: browser-sync
- depends only on browser-sync (npm install it globally) and the `bs-config.js` file (`host` needs to be edited manually!)
- uses its built-in static server (actually it's a connect middleware)
- to start, run `browser-sync start`

## method 2: connect
- sets up a slightly more complex connect app
- requires 4 node modules (run `npm install`) and the `server.js` file (`host` needs to be edited manually)
- to start, run `node server.js`
